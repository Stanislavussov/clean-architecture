# Clean architecture in the frontend

This post is translation, therefore there would be some grammar mistakes.

* [Application source](https://github.com/bespoyasov/frontend-clean-architecture)

## Plan

First, we will talk about what a clean architecture is in general and get acquainted with such concepts as a domain, a user case, and application layers. Then we will discuss how this applies to the frontend and whether it is worth bothering at all.

Next, we will design the front-end of the cookie shop following the precepts of clean architecture. This store will use React as a UI framework to show that this approach to architecture applies to it too. Then we will implement one of the user scenarios from scratch to see if it is convenient.

There will be TypeScript in the code, but only to show how to use types and interfaces to describe entities. 

Today we will hardly talk about OOP, we will mention OOP only once at the end.

Please note that the examples in the post are intentionally simple to make it easier to understand the concept and idea of ​​a clean architecture. This post is not a “write code this way and nothing else” instruction, but rather a gallery of approaches that you can choose and use in your projects.

At the end of the post there will also be a list of methodologies that are close in spirit to pure architecture and everything that will be in this post. Some of them are already widely used in the frontend, so you will have the opportunity to choose what suits your project best.

And now — let's get to work!

## Architecture and design

> Designing is fundamentally about taking things apart... in such a way that they can be put back together. ...Separating things into things that can be composed that's what design is.
— Rich Hickey. [Design Composition and Performance](https://www.infoq.com/presentations/Design-Composition-Performance/)

System design, says the quotation in the epigraph, is the division of a system in such a way that it can then be assembled again. And most importantly - easy to assemble, without unnecessary labor.

I agree: building a house out of cubes is easier than breaking a large boulder into pebbles. But I consider system extensibility to be one of the goals of a competent architecture. Program requirements are constantly changing. We want the program to be easy to update and modify to meet new requirements. A clean architecture can help achieve this goal.

## Clean architecture

A [clean architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) is a way of separating responsibilities and pieces of functionality according to how close they are to the application's domain.

By subject area , we mean the part of the real world that we model with the program. These are data transformations that reflect transformations in the real world. For example, if we updated the product name, then replacing the old name with a new one is a domain transformation.

A clean architecture is often referred to as a three-layer architecture because it divides the application into layers. The original post about  The Clean Architecture has a diagram with layers highlighted:

![image-2.png](./image-2.png)

*A diagram of layers according to a pure architecture: in the center is the domain, around it is the application layer, and outside is the layer of adapters*

### Domain layer
In the center is the [domain layer](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/#domain-layer). These are the entities and data that describe the subject area of ​​the application, as well as the code for transforming this data. A domain is the core that distinguishes one application from another.

You can think of a domain as something that will definitely not change if we move from React to Angular, or if we change some user script. In the case of a store, these are products, orders, users, a shopping cart, and functions to update their data.

The data structure of domain entities and the essence of their transformations do not depend on external circumstances. External circumstances trigger domain transformations, but do not determine how they will proceed.

The function of adding goods to the cart does not matter how exactly the goods were added: by the user himself through the "Buy" button or automatically by the promo code. In both cases, it will accept the product and return an updated cart with the added product.

### Application layer
[The application layer](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/#application-layer) is located around the domain. This layer describes user cases - that is, user scenarios. They are responsible for what happens after the occurrence of some event.

For example, the scenario “Put an item in the cart” is a use case. It describes the actions that should happen after the button is clicked. This is such an "orchestrator" who says:

* now go to the server, send such a request;
* now perform such and such a domain transformation;
* now redraw the UI using the new data.

Also in the application layer are [ports](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/#ports) - specifications of how our application wants the outside world to communicate with it. Typically, a port is an interface, a contract for behavior.

Ports serve as a "buffer" between the Wishlist of our application and the realities of the outside world. Input Ports tell how the application wants to be accessed from the outside. Output Ports tell how the application is going to communicate with the outside world so that it is ready for it.

We'll look at ports and their uses in more detail later.

### Layer of adapters
The outermost layer contains [adapters](https://herbertograca.com/2017/09/14/ports-adapters-architecture/) for external services. Adapters are needed to turn an incompatible API of external services into a compatible one with the wishes of our application.

Adapters are a great way to reduce the coupling between our code and third-party service code. Low engagement reduces the need to change one module when changing others.

Adapters are often divided into:

* driving - which send signals to our application;
* driven - which receive signals from our application.

Driving adapters are the most frequently user-interacted. For example, handling a button click by a UI framework is the job of a driving adapter. It works with the browser API (essentially a third-party service) and converts the event into a signal that our application understands.

Driven adapters interact with the infrastructure. In the frontend, most of the infrastructure is a backend server, but sometimes we can interact with some other services directly, for example, with a search engine.

Please note that the further we are from the center, the more "service" the functionality of the code, the further it is from the subject area of our application. This will be important later when we decide which layer to assign a module to.

### Dependency Rule
The three-layer architecture has a dependency rule: only outer layers can depend on inner ones . It means that:

the domain must be independent;
the application layer may be domain dependent;
outer layers can depend on anything.
Outer layers can depend on inner layers, but not vice versa
Outer layers can depend on inner layers, but not vice versa

![image.png](./image.png)

*The outer layers may depend on the inner ones, but not vice versa*

Sometimes this rule can be neglected, although it is better not to abuse it. For example, sometimes it is convenient to use some "semi-library" code in the domain, although according to the canons there should not be any dependencies. We will look at an example of such a violation when we get to the code itself.

Messy dependency routing can lead to complex and confusing code. For example, a violation of the dependency rule can result in:

* To cyclic dependencies, when the module A depends on B, B - on C, and C - on A. It is difficult to resolve such cycles.
* To poor testability, when in order to test a small part, you have to simulate the operation of the entire system.
* To too high engagement, and as a result, to fragile interaction between the modules.

## Benefits of a clean architecture

Now let's talk about what this division of code gives us. It has several benefits.

### Detached domain
All the main functionality of the application is isolated and collected in one place - in the domain. Functionality in a domain is independent, which means it is easier to test. The less dependencies a module has, the less infrastructure is needed for testing, the less mocks and stubs are needed.

A stand-alone domain is also easier to check against business expectations. This helps new developers quickly get to grips with what the application needs to do. In addition, a separate domain helps to quickly find errors and inaccuracies in the "translation" from the business language to the programming language.

### Independent use cases
Application scenarios, use cases, are described separately. It is they who dictate which third-party services will be needed. We adjust the outside world to our needs, and not vice versa - this gives more freedom in choosing third-party services. For example, we can quickly change the payment system if the current one began to require too much commission.

Also, the use case code is flat, testable and extensible. We will see this with an example later.

### Replaceable Third Party Services
External services become replaceable thanks to adapters. As long as we do not change the interface for interacting with the application, it does not matter to us which external service will implement this interface.

Thus, we create a barrier to the spread of changes: changes in someone else's code do not directly affect ours. Adapters also limit  the propagation of errors while the application is running.

## The costs of a clean architecture
Architecture is first and foremost a tool. Like any tool, clean architecture has costs in addition to benefits.

### Takes time
The main cost is time. You will need it not only for design, but also for implementation, because it is always easier to call a third-party service directly than to write adapters.

Thinking through the interaction of all system modules in advance is also difficult, because we may not know in advance all the requirements and restrictions. When designing, keep in mind how the system can change and leave room for expansion.

### Sometimes too wordy
In general, the canonical embodiment of pure architecture is not always convenient, and sometimes even harmful. If the project is small, then the full implementation will be an overhead, which will increase the entry threshold for beginners.

Design compromises may need to be made to stay within budget or deadline.

### Raises the entry threshold
A full implementation of a clean architecture can increase the barrier to entry and flatten the learning curve, because any tool requires knowing how to use it.

If you overengineer at the start of a project, then it will be more difficult to onboard new developers. You need to keep this in mind and keep the code simple.

### Increases the amount of code
Specifically, the front-end problem is that a clean architecture can increase the amount of code in the final bundle. The more code we give to the browser, the more it downloads, parses and interprets.

You have to keep track of the amount of code and make decisions about where to cut corners:

* maybe a little easier to describe the use case;
* maybe access the domain functionality directly from the adapter, bypassing the use case;
* you may need to set up code splitting, etc.

## How to reduce costs
You can reduce the amount of time and code by cutting corners and sacrificing a little canonicity. I am generally not a fan of radicalism in approaches: if it is more pragmatic (benefits > potential costs) to break the rule, I will break it.

So, you can forget about some aspects of clean architecture for some time without visible problems. The minimum necessary, what you should pay attention to when designing, are two things.

### Allocate a domain
It is the dedicated domain layer that helps to understand what we are designing in general and how it should work. A dedicated domain makes it easier for new developers to understand the essence of the application, its entities, and the relationships between them.

Even if we skip the rest of the layers and put spaggetti-code into production, it will still be easier to work and refactor with a dedicated domain that is not spread over the codebase. Other layers can be added as needed.

### Follow the dependency rule
The second rule that you should not refuse is the rule of dependencies, or rather their direction. External services should adapt to us and never vice versa.

If you feel like you're "finishing" your code so that it can call the search API, something is wrong. Better write an adapter before the problem metastasizes.

## Designing an application
Now that we have talked about the theory, we can begin to practice. Let's design the architecture of a cookie shop as an example .

The store will sell different types of cookies, which may have different composition. Users will choose cookies and order them, and pay for orders in a third-party payment service.

On the main page there will be a showcase with cookies that we can buy. We will be able to buy cookies only if we are authenticated. The login button will take us to the login page, where we can log in.

![Screenshot from 2022-08-09 21-19-28.png](./Screenshot from 2022-08-09 21-19-28.png)


*Home page*

After a successful login, we will be able to put some cookies in our basket.

![Screenshot from 2022-08-09 21-21-07.png](./Screenshot from 2022-08-09 21-21-07.png)

*Basket with selected cookies*

When we put cookies in the basket, we can place an order. After payment, we receive a new order in the list and an empty basket.

Placing an order will be the very scenario that we be shown in this post. Other use case you can find on source code.

To start designing, we first decide what entities, scenarios and functionality in the broadest sense we will have at all. Then we decide which layer they should belong to.

### Designable domain
The most important thing in an application is the domain. It is in it that the main entities of the application and the transformations of their data will be located. I propose to start designing from it in order to reflect the subject area of ​​the store in the code as accurately as possible.

The domain includes:

* data types of each entity: user, cookie, cart and order;
* factories for creating each entity, or classes if we write in OOP;
* and functions for transforming this data.

The transform functions in a domain should only depend on the rules of the domain and nothing else. Such functions would be, for example:

* the function of calculating the total cost;
* determination of the user's taste preferences
* determining if an item is in the cart, etc.

![image-1.png](./image-1.png)

*Diagram of domain entities in the inner layer*

### Designing the application layer
The application layer contains use cases - user scripts. A script always has an actor, usually a user, an action, and a result.

For example, we can highlight:

* product purchase scenario;
* payment, calling third-party payment systems;
* interaction with goods and orders: updating, viewing;
* access to pages depending on roles.

Scenarios are usually described in terms of the subject area. For example, the "checkout" scenario actually consists of several steps:

* get items from the cart and create a new order;
* pay for the order;
* notify the user if the payment failed;
* empty cart and show order.

The use case function will be the code that describes this scenario.

Also in the application layer are port interfaces for communicating with the outside world.

![image-3.png](./image-3.png)

*Diagram of use cases and ports in the middle layer*

### Designing the adapter layer
In the adapter layer, we keep adapters to external services. The task of adapters is to make the incompatible API of third-party services compatible with our Wishlist.

In the frontend, most often this is a UI framework and a request module to the API server. In our case, among the adapters, we will highlight:

* UI framework;
* API request module;
* adapter for local storage;
* API response adapters and converters to the application layer.

![image-4.png](./image-4.png)

*Diagram of adapters divided into driving and driven*

Note that the more "service" functionality, the further it is from the center of the diagram. The main part of the application is located in the center, it is the domain that contains the business logic and carries business value, and everything else is the service code.

### Using the MVC analogy
Sometimes it is difficult to immediately decide which layer to attribute some module or data to. A small (and incomplete!) analogy with MVC can help here:

* model is usually domain entities,
* controllers are domain transformations and application layer,
* the view is the control adapters.

The ideas differ in details, but are conceptually similar, and this analogy can be used to define domain and application code.

## Detailing the design: domain

When we have decided on what entities we need, we can begin to determine how they behave.

In order not to be distracted further, I will immediately show the structure of the code in the project. For clarity, I divide the code into folders-layers.

```
src/
|_domain/
  |_user.ts
  |_product.ts
  |_order.ts
  |_cart.ts
|_application/
  |_addToCart.ts
  |_authenticate.ts
  |_orderProducts.ts
  |_ports.ts
|_services/
  |_authAdapter.ts
  |_notificationAdapter.ts
  |_paymentAdapter.ts
  |_storageAdapter.ts
  |_api.ts
  |_store.tsx
|_lib/
|_ui/

```

The domain is in domain/, the application layer is in  application/, and the adapters are in  services/. I will talk about alternatives to such code splitting at the end.

### We write domain entities
In the domain we will have 4 modules:

* product;
* user;
* order;
* basket.

The main actor is the user. We will store user data in storage during the session. We want to type this data, so let's create a domain user type.

The user type will contain an id, name, email, and lists of preferences and allergies.

```
// domain/user.ts

export type UserName = string;
export type User = {
  id: UniqueId;
  name: UserName;
  email: Email;
  preferences: Ingredient[];
  allergies: Ingredient[];
};
```

Users will add cookies to the cart, we will add types for the cart and product. The product will contain an identifier, a name, a price in kopecks, and a list of ingredients.


```
// domain/product.ts

export type ProductTitle = string;
export type Product = {
  id: UniqueId;
  title: ProductTitle;
  price: PriceCents;
  toppings: Ingredient[];
};
```

In the cart, we will only keep a list of the products that the user put in it:

```
// domain/cart.ts

import { Product } from "./product";

export type Cart = {
  products: Product[];
};
```

After successful payment, an order is created with the specified cookies, let's create the order entity.

The order type will contain the user ID, the list of ordered products, the creation date and time, the status, and the total price for the entire order.

```
// domain/order.ts

export type OrderStatus = "new" | "delivery" | "completed";

export type Order = {
  user: UniqueId;
  cart: Cart;
  created: DateTimeString;
  status: OrderStatus;
  total: PriceCents;
};
```

### Checking relationships between entities

![image-5.png](./image-5.png)

*Entity relationship diagram*

We can make sure if the main actor is the user, if there is enough information in the order, if some entity needs to be extended, if there will be problems with extensibility in the future.

If everything matches our expectations, then we can start designing domain transformations.

### Creating data transformations
Anything will happen to the data we just designed the types for. We will be adding products to the cart, emptying it, updating products and usernames, and so on. For all such transformations, we will create separate functions.

For example, to determine if a user is allergic to an ingredient or preference, we can write functions `hasAllergyand` and `hasPreference`:

```
// domain/user.ts

export function hasAllergy(user: User, ingredient: Ingredient): boolean {
  return user.allergies.includes(ingredient);
}

export function hasPreference(user: User, ingredient: Ingredient): boolean {
  return user.preferences.includes(ingredient);
}
```

To add items to the cart and check if the item is in the cart, use the addProductand functions contains:

```

// domain/cart.ts

export function addProduct(cart: Cart, product: Product): Cart {
  return { ...cart, products: [...cart.products, product] };
}

export function contains(cart: Cart, product: Product): boolean {
  return cart.products.some(({ id }) => id === product.id);
}
```

We also need to calculate the total price of the list of products - we will write a function for this totalPrice. If desired, we can add to this function the accounting for various conditions, such as promo codes or seasonal discounts.

```
// domain/product.ts

export function totalPrice(products: Product[]): PriceCents {
  return products.reduce((total, { price }) => total + price, 0);
}
```

In order for users to place orders, we will add a function createOrder. When called, it will return a new order assigned to the specified user.

```
// domain/order.ts

export function createOrder(user: User, cart: Cart): Order {
  return {
    cart,
    user: user.id,
    status: "new",
    created: new Date().toISOString(),
    total: totalPrice(products),
  };
}
```

Let's pay attention that in each function we build the API so that it is convenient for us to transform the data. We accept the argument and give the result in the form in which we want and conveniently.

At the domain design stage, there are no external restrictions yet. This allows you to reflect data transformations as close as possible to the subject area. And the closer the transformations are to reality, the easier it will be to check the work.

## Detailing the Design: Shared Kernel

You may have noticed some of the types we used in describing domain types. For example, Email, UniqueIdor DateTimeString. This is an alias type :

```
// shared-kernel.d.ts

type Email = string;
type UniqueId = string;
type DateTimeString = string;
type PriceCents = number;
```


I usually use type aliases to get rid of [the obsession with primitive types (primitive obsession)](https://refactoring.guru/smells/primitive-obsession).

I use not just string, but  DateTimeStringto make it clearer what kind of string is used. The closer the type is to the domain, the easier it will be to deal with errors when they appear.

The specified types are in the `shared-kernel.d.ts.` [Shared kernel](http://ddd.fed.wiki.org/view/welcome-visitors/view/domain-driven-design/view/shared-kernel) is such code and data, dependence on which does not increase coupling (coupling) between modules. I advise you to read more about the concept in [“DDD, Hexagonal, Onion, Clean, CQRS, … How I put it all together” ](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/).

In practice, the shared kernel can be explained as follows. We use TypeScript itself, use its standard type library, but don't consider it a dependency. This is because the modules that use it may not know anything about each other and remain decoupled.

Not every code can be attributed to a shared kernel. The main and most important limitation is that such code must be compatible with any part of the system. If part of the application is written in TypeScript and part is in another language, the shared kernel may contain only code that can be used in both parts. For example, entity specifications in JSON format are suitable, TypeScript helpers are no longer available.

In our case, the entire application is written in TypeScript, so the type-aliases over the built-in types can also be attributed to the shared kernel. Such globally available types do not increase the engagement between modules and can be used in any part of the application.

## Detailing the application layer
We figured out the domain, you can proceed to the application layer. This layer contains user cases or user scripts.

In the code of the use cases, we describe the technical details of how the scenario works. What should happen to the data after adding a product to the cart or placing an order is a use case.

Use cases involve interaction with the outside world, which means the use of external services. Interaction with the outside world is a side effect. We know it's easier to program and debug features and systems without side effects. And most of the domain functions we have already written as pure functions. To make pure transformations and interaction with the "impure" world friendly, we can use the application layer as an impure context.

### Using impure context for pure transformations
An impure context for pure transformations is a code organization in which:

* we first do a side effect to get the data;
* then we perform a clean transformation on this data;
* and then again we produce a side effect to save or transmit the result.

Using the example of the “Put product in the cart” scenario, it would look like this:

* first, the handler will receive data about the state of the basket from the storage;
* will then call the update cart function, passing in the item to be added;
* then save the updated cart to storage.

It turns out such a "sandwich": side effect, pure function, side effect. The main logic is reflected in data transformation, and all communication with the world is isolated in an imperative shell.

![image-6.png](./image-6.png)

*Functional architecture: side effect, pure function, side effect*

An impure context is sometimes called a functional core in an imperative wrapper. Mark Siemann [wrote](https://blog.ploeh.dk/2020/03/02/impureim-sandwich/) about this in his blog. We will use this approach when writing the use-case functions.

### Designing the scenario
From all application scenarios, we will select and design a checkout scenario. It is the most indicative, because it is asynchronous and pulls a lot of third-party services.

Let's think about what we want to achieve in the interesting use case. The user has a basket of cookies. When the user clicks the checkout button:

* we want to create a new order;
* pay it in a third-party payment system;
* if the payment failed, inform the user about it;
* if passed, save the order on the server;
* add order to local data store to show on screen.

From an API and function signature point of view, we want to pass the user and cart as arguments, and have the function do the rest for itself.

```
type OrderProducts = (user: User, cart: Cart) => Promise<void>;
```

Ideally, of course, the user case should not take 2 separate arguments, but a command that will encapsulate all the input data inside itself. But we don't want to inflate the amount of code, so we'll leave it at that.

### Writing Application Layer Ports
Let's take a closer look at the stages of the use case: the creation of an order itself is a function from the domain. Everything else is external services that we want to use.

It is important to remember that it is external services that should adapt to our needs and not vice versa. Therefore, in the application layer, we will describe not only the user case itself, but also the interfaces for these external services - ports.

Ports should be convenient for our application in the first place. If the API of external services is incompatible with our whishes, we will write an adapter.

Let's figure out what services we need:

* service for payment of orders;
* to notify the user about events and errors;
* to save data to local storage.

![image-7.png](./image-7.png)

*Necessary services for the script to work*

Please note that now we are talking about the  interfaces of these services, and not their implementation. At this stage, it is important for us to describe the required behavior, because we will rely on this behavior in the application layer when describing the scenario.

How exactly this behavior will be implemented is not important to us yet. The main thing is that the implementation fulfills the guarantees that are described in the interfaces. This allows us to defer the decision about which external services to use until the very last moment - that is, it keeps the code minimally hooked. We will deal with the implementation later.

Also note that we divide interfaces by features. Everything related to payments - in one module, related to the data storage - in another. This will make it easier to ensure that the functionality of different third-party services [does not mix ISP](https://en.wikipedia.org/wiki/Cohesion_(computer_science)) in all its glory.

###  Declare the interface of the payment system
The store is a sample application, so the payment system will be as simple as possible. She will have a method `tryPay` that will accept the amount of money that needs to be paid, and in response will send confirmation that everything is fine.

```
// application/ports.ts

export interface PaymentService {
  tryPay(amount: PriceCents): Promise<boolean>;
}
```

We will not process errors, because error handling is a topic for a whole separate big post 😃

Yes, usually, payment is made on the server, but this is a toy example, we will do everything on the client. We could easily talk to our API instead of the payment directly instead. Such a change, by the way, would only affect this user case, the rest of the code would remain untouched.

### Declaring the notification service interface
If something doesn't go according to plan, you need to tell us about it.

The user can be notified in different ways. You can use a block in the UI, you can send letters, you can vibrate your phone (please don't). In general, it would also be better for the notification service to be abstract, so that now you don’t have to think about the implementation of these notifications.

Let's take a message and somehow notify the user:

```
// application/ports.ts

export interface NotificationService {
  notify(message: string): void;
}
```

### Declaring the local storage interface
We will save the new order in the local storage.

This repository can be anything: Redux, MobX, whatever-floats-your-boat-js. The storage can be divided into micro-stores for different entities, or be one large store for all application data - this is also not important now, because these are implementation details. It is important for us to design the interface.

I like to divide storage interfaces into separate ones for each entity. A separate interface for the user data store, a separate one for the shopping cart, a separate one for the order store:

```
// application/ports.ts

export interface OrdersStorageService {
  orders: Order[];
  updateOrders(orders: Order[]): void;
}
```


### We write the script code
Let's check if we can build the script using the created interfaces and the existing domain functionality. As we described earlier, the script will consist of the following steps:

* checking the data;
* create an order;
* pay for the order;
* reporting problems;
* save the result.

![image-8.png](./image-8.png)

*All steps of the custom script in the diagram*

Let's get to the code. First, let's declare the stubs for the services we are going to use. TypeScript will complain that we haven't implemented the interfaces in the appropriate variables, but that doesn't matter for now.

```
// application/orderProducts.ts

const payment: PaymentService = {};
const notifier: NotificationService = {};
const orderStorage: OrdersStorageService = {};
```

Now we can use the created stubs as if they were real services - access their fields, call their methods. This will be handy when "translating" a script from a domain language to TypeScript.

Let's create a usecase function `orderProducts`. Inside, first of all, we create a new order:

```
// application/orderProducts.ts
//...

async function orderProducts(user: User, cart: Cart) {
  const order = createOrder(user, cart);
}
```

We then use the generated stubs to call the desired methods of each service. Here we use the fact that an interface is a guarantee, a contract for behavior. This means that in the future, stub variables will actually do what we are now expecting:

```
// application/orderProducts.ts
//...

async function orderProducts(user: User, cart: Cart) {
  const order = createOrder(user, cart);

  // Try to pay order;
  // Notify user if something is going wrong:
  const paid = await payment.tryPay(order.total);
  if (!paid) return notifier.notify("Оплата не прошла 🤷");

  // Save result and clean the basket
  const { orders } = orderStorage;
  orderStorage.updateOrders([...orders, order]);
  cartStorage.emptyCart();
}
```

Please note that the use case does not call third-party services directly. It relies on the behavior described in the interfaces, so as long as the interface remains the same, we don't care which module implements it and how. This makes the modules replaceable.

## Detailing the adapter layer
We "translated" the script to TypeScript. Now we need to check whether the reality matches our wishes from the interfaces.

Usually - no, it does not match. So we tailor the outside world to our needs with [adapters](https://en.wikipedia.org/wiki/Adapter_pattern) .

### Associating UI and usecase
The first adapter is a UI framework or library. It links the native browser API and the application. In the case of an order creation usecase, these are the Checkout button and a click handler that will launch the usecase function.

```

// ui/components/Buy.tsx

export function Buy() {
  // Get acces to use case in component
  const { orderProducts } = useOrderProducts();

  async function handleSubmit(e: React.FormEvent) {
    setLoading(true);
    e.preventDefault();

    // Call use case function:
    await orderProducts(user!, cart);
    setLoading(false);
  }

  return (
    <section>
      <h2>Take order</h2>
      <form onSubmit={handleSubmit}>{/* ... */}</form>
    </section>
  );
}
```

Now it's fashionable to write on hooks, so let's provide access to the user case through a hook. Inside we will get all the services, and as a result from the hook we will return the very function of the use case.

```
// application/orderProducts.ts

export function useOrderProducts() {
  const notifier = useNotifier();
  const payment = usePayment();
  const orderStorage = useOrdersStorage();

  async function orderProducts(user: User, cookies: Cookie[]) {
    // …
  }

  return { orderProducts };
}
```

We can say that we use hooks as "artisanal dependency injection". First, using the `useNotifier`, `usePayment`, `useOrdersStorage` hooks, we get service instances, and then we use the closure of the useOrderProducts function so that they are available inside the orderProducts function.

It's important to note that the usecase function is still separate from the rest of the code, which is important for testing purposes. We will pull it out completely and make it even more testable at the end of the article when we review and refactor.

### Implementing a payment service
The usecase uses the interface `PaymentService`. Let's write its implementation.

For payment, we will use a fake API stub. Nothing forces us to write a service now, it will be possible to write it later, the main thing is to implement the specified behavior:

// services/paymentAdapter.ts

```
import { fakeApi } from "./api";
import { PaymentService } from "../application/ports";

export function usePayment(): PaymentService {
  return {
    tryPay(amount: PriceCents) {
      return fakeApi(true);
    },
  };
}
```

The function `fakeApiis` a timeout that fires after 450ms, simulating a delay in the response from the server. It returns what we pass to it as an argument.

```
// services/api.ts

export function fakeApi<TResponse>(response: TResponse): Promise<TResponse> {
  return new Promise((res) => setTimeout(() => res(response), 450));
}
```

We explicitly type the return value of `usePayment`. This is how TypeScript checks that the function actually returns an object that contains all the methods declared in the interface.

### Implementing a notification service
Let notifications be simple alerts. Since the code is decoupled, there will be no problem rewriting this service later.

```
// services/notificationAdapter.ts

import { NotificationService } from "../application/ports";

export function useNotifier(): NotificationService {
  return {
    notify: (message: string) => window.alert(message),
  };
}
```

Implementing local storage
Let the storage will be React.Context. Let's create a context, pass a value to the provider, and export the provider and access to the store through hooks.

```
// store.tsx

const StoreContext = React.createContext({});
export const useStore = () => useContext(StoreContext);

export const Provider: React.FC = ({ children }) => {
  const [orders, setOrders] = useState([]);

  const value = {
    // ...
    orders,
    updateOrders: setOrders,
  };

  return <StoreContext.Provider value={value}>{children}</StoreContext.Provider>;
};
```

We will write a hook for accessing the storage separately for each feature. This way we will not break [ISP](https://en.wikipedia.org/wiki/Interface_segregation_principle), and the stores, at least in terms of interfaces, will be atomic.

```
// services/storageAdapter.ts

export function useOrdersStorage(): OrdersStorageService {
  return useStore();
}
```

Also, this approach will give us the opportunity to set up additional optimizations for each store: selectors, memoizations, and so on.

## Validating the data flow scheme
Let's now check how the user will communicate with the application during the created scenario. On the diagram we will show what data will come from, where and where to go.

![image-9.png](./image-9.png)

*Scenario Data Flow Diagram*

The user interacts with the UI layer, which can only access the application through ports. That is, we can change the UI if we want.

Scenarios are processed in the application layer, which says exactly how external services that may be needed can interact with it. All main logic and data is in the domain.

All external services are hidden in the infrastructure and obey our specifications. If you need to change the service for sending messages, the only thing that needs to be corrected in the code is the adapter for the new service.

Such a scheme makes the code replaceable, testable and extensible under changing requirements.

## What can be improved
In general, for a start and an initial understanding of the clean architecture, what you have read is already enough. But I want to draw attention to things that I have simplified to make the story easier. This section is optional, but will give you a broader understanding of what code looks like when it comes to a clean, "no cut corners" architecture.

I would highlight a few things that hurt the eyes and that can be improved.

Use object instead of number for price
You may have noticed that I use a number to describe the price. This is not very good practice.

```
// shared-kernel.d.ts

type PriceCents = number;
The number indicates only the quantity, but does not indicate the currency, and the price without the currency does not make sense. In a good way, the price should be made an object with two fields: value and currency.

type Currency = "RUB" | "USD" | "EUR" | "SEK";
type AmountCents = number;

type Price = {
  value: AmountCents;
  currency: Currency;
};
```

This will solve the problem of currency storage and save a lot of effort and nerves when changing or adding currencies in the store. I didn't use this type in the examples to keep things simple. In real code, the price would be similar to this type.

Separately, it is worth mentioning the value of the price. I always keep the amount of money in the smallest fraction of the currency that is in circulation. For example, for the dollar - cents.

Such a price display allows you not to think about division and fractional values. With money, this is especially important if we want to avoid problems with floating point and fractional prices.

Divide code by features, not layers
The code can be arranged and placed in folders not “by layers”, but “by features”. One "feature" will be a piece of cake from the diagram below. Such a separation is even preferable, because it allows you to deploy features separately , and this is often useful.

![image-11.png](./image-11.png)

*A component is a piece of a hexagonal pie*

I advise you to read about how to divide the code into similar components in  [“DDD, Hexagonal, Onion, Clean, CQRS, … How I put it all together” ](https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/#components) - both the benefits and the costs of division are explained there. I also advise you to look at [Feature Sliced](https://feature-sliced.design/) ​, which is conceptually very similar to component code division, but easier to understand.

### Pay attention to cross-component usage
If we are talking about division into components, it is worth mentioning the cross-component use of code. Let's remember the order creation function:

```
import { Product, totalPrice } from "./product";

export function createOrder(user: User, cart: Cart): Order {
  return {
    cart,
    user: user.id,
    status: "new",
    created: new Date().toISOString(),
    total: totalPrice(products),
  };
}
```

This function uses `totalPricefrom` the "other component" - the product. In itself, such a use is normal, but if we want to divide the code into independent features, then it will be impossible to access the functionality of another feature directly.

### Use Branded Types, not aliases
For the shared kernel, I used type aliases. They are good because they are easy to operate: it is enough to create a new type and refer, for example, to a string. But their disadvantage is that there is no mechanism in TypeScript to monitor their use and enforce it.

It seems that this is not a problem: well, someone uses it `string` instead `DateTimeString`- so what? the code will be collected.

The problem is that the code will be assembled, although a wider type is used (with smart words, the [precondition is weakened](https://en.wikipedia.org/wiki/Design_by_contract)). This primarily makes the code more brittle, because it allows any strings to be used, not just special-quality strings, which can lead to bugs.

And secondly, it's confusing to read because it creates two sources of truth. It is not clear whether only the date really needs to be used there, or, in principle, any strings can be used.

There is a way to make TypeScript understand that we want a specific type - branded types. Branding makes it possible to keep track of exactly how the types are used, but makes the code a little more complicated.

### Pay attention to a possible "dependency" in the domain
The next moment that hurts the eye is the creation of a date in the domain in the function `createOrder`:

```
import { Product, totalPrice } from "./product";

export function createOrder(user: User, cart: Cart): Order {
  return {
    cart,
    user: user.id,

    // This line:
    created: new Date().toISOString(),

    status: "new",
    total: totalPrice(products),
  };
}
```

There is a suspicion that it new Date().toISOString()will be repeated quite often in the project and I want to put it in a "helper":

// lib/datetime.ts

```
export function currentDatetime(): DateTimeString {
  return new Date().toISOString();
}
...And then use it in the domain:

// domain/order.ts

import { currentDatetime } from "../lib/datetime";
import { Product, totalPrice } from "./product";

export function createOrder(user: User, cart: Cart): Order {
  return {
    cart,
    user: user.id,
    status: "new",
    created: currentDatetime(),
    total: totalPrice(products),
  };
}
```

But we immediately remember that you can’t depend on anything in a domain - what to do? In a good way, `createOrderit` should accept data for the order in a ready-made form, the date can be passed as the last argument:

```
// domain/order.ts

export function createOrder(user: User, cart: Cart, created: DateTimeString): Order {
  return {
    cart,
    user: user.id,
    status: "new",
    created,
    total: totalPrice(products),
  };
}
```

This also allows you not to violate the dependency rule in cases where the creation of a date depends on libraries . If we create the date “outside” the domain function, then most likely this date will be created inside the user case and passed as an argument:

```
function someUserCase() {
  // Using adapter `dateTimeSource`,
  const createdOn = dateTimeSource.currentDatetime();

  // Transfer created date to domain function
  createOrder(user, cart, createdOn);
}
```

This way, the domain will remain independent, and besides, it will be easier to test.

In the examples, I decided not to focus on this for two reasons: it would be distracting from the point, and I don't see anything wrong with depending on your own helper, which uses only the features of the . Such helpers can even be placed in the project's shared kernel, because they only reduce code duplication.

### Keep domain entities and transformations clean
But what createOrderwas really not very good in creating a date inside the function is a side effect. The problem with side effects is that they make the system less predictable than we would like. Clean transformations in the domain, that is, those that do not produce side effects , help to cope with this .

Creating a date is a side effect, because at different times the result of the call is Date.now()different. A pure function always returns the same result with the same arguments.

I've come to the conclusion that it's best to keep the domain as clean as possible. This makes it easier to test, easier to port and update, easier to read. Side effects dramatically increase the cognitive load when debugging, and the domain is not the place to keep complex and obfuscated code.

Pay attention to the relationship between cart and order
This little example Orderincludes a cart because the cart only represents a list of products:

```
export type Cart = {
  products: Product[];
};

export type Order = {
  user: UniqueId;
  cart: Cart;
  created: DateTimeString;
  status: OrderStatus;
  total: PriceCents;
};
```

This may not work if there are additional properties in the cart that are not related to the order in any way. In such cases, it is better to use data projections or intermediate DTOs .

Alternatively, you can use the "Product List" entity:

```
type ProductList = Product[];

type Cart = {
  products: ProductList;
};

type Order = {
  user: UniqueId;
  products: ProductList;
  created: DateTimeString;
  status: OrderStatus;
  total: PriceCents;
};
```

Make the user case more testable
There is also something to discuss in the user case. Now the function is orderProductsdifficult to test in isolation from React - this is bad. Ideally, it should be testable with a minimum amount of effort.

The problem with the current implementation in the hook that provides access to the user case in the UI:

```
// application/orderProducts.ts

export function useOrderProducts() {
  const notifier = useNotifier();
  const payment = usePayment();
  const orderStorage = useOrdersStorage();
  const cartStorage = useCartStorage();

  async function orderProducts(user: User, cart: Cart) {
    const order = createOrder(user, cart);

    const paid = await payment.tryPay(order.total);
    if (!paid) return notifier.notify("Payment not succedeed 🤷");

    const { orders } = orderStorage;
    orderStorage.updateOrders([...orders, order]);
    cartStorage.emptyCart();
  }

  return { orderProducts };
}
```

In canonical execution, the usercase function would be moved outside the hook, and services would be passed to the usercase via the last argument or using DI:

```
type Dependencies = {
  notifier?: NotificationService;
  payment?: PaymentService;
  orderStorage?: OrderStorageService;
};

async function orderProducts(
  user: User,
  cart: Cart,
  dependencies: Dependencies = defaultDependencies,
) {
  const { notifier, payment, orderStorage } = dependencies;

  // ...
}
```

The hook in this case would turn into an adapter:

```
function useOrderProducts() {
  const notifier = useNotifier();
  const payment = usePayment();
  const orderStorage = useOrdersStorage();

  return (user: User, cart: Cart) =>
    orderProducts(user, cart, {
      notifier,
      payment,
      orderStorage,
    });
}
```

Then the hook code could be attributed to adapters, and only the use case would remain in the application layer. The function `orderProducts` can be tested by passing mocks of the necessary services as dependencies.

### Set up automatic dependency injection
In the same place, in the application layer, we now “implement” services with our hands:

```
export function useOrderProducts() {
  // Здесь мы используем хуки, чтобы получить инстансы каждого сервиса,
  // который будем использоовать внутри юзкейса orderProducts:
  const notifier = useNotifier();
  const payment = usePayment();
  const orderStorage = useOrdersStorage();
  const cartStorage = useCartStorage();

  async function orderProducts(user: User, cart: Cart) {
    // ...Внутри юзкейса используем сервисы.
  }

  return { orderProducts };
}
```

But in general this can be automated and done through dependency injection. We have already covered the simplest version of "injection" through the last argument, but you can go further and set up automatic injection.

Specifically in this application, I thought that setting up DI didn’t make much sense. It would distract from the essence and overcomplicate the code. And in the case of React and hooks, we can use them as a "container" that returns the implementation of the specified interface. Yes, this is manual work, but it does not increase the entry threshold and is faster for beginners to read.

## What can be more difficult in real projects
The example from the post is refined and intentionally simple. It is clear that life is much more amazing and confusing than this example. Therefore, I would like to talk about common problems that may arise when working on a clean architecture.

Branching business logic
The main problem is the subject area, about which we lack knowledge. Imagine that the store has a product, a promotional product, and a retired product. How to properly describe these entities?

Should there be a "base" entity that will be extended? How exactly to expand this entity? Should there be additional fields? Should these entities be mutually exclusive? How should user cases behave if there is another one instead of a simple product? Should we immediately reduce duplication?

There may be too many questions, and there may not be any answers, because no one from the team and stakeholders knows how the system should actually behave. If there are only assumptions, then you will have to make a decision while in [paralysis of analysis](https://en.wikipedia.org/wiki/Analysis_paralysis) , which is very difficult.

Specific solutions depend on the specific situation, I can only recommend a few general ones.

Don't use inheritance, even if it's called "extension", even if it seems like the interface is really inherited, even if it seems like "well, there's obviously a hierarchy right there". Just wait.

Copy-paste in code is not always evil , it's a tool. Make two almost identical entities, see how they behave in reality, watch them. At some point, you will notice that they either become very different, or they really differ in only one field. Merging two similar entities into one is easier than smearing the code with test conditions for each possible option.

If you still need to expand...

Be aware of [covariance, contravariance, and invariance](https://en.wikipedia.org/wiki/Covariance_and_contravariance_(computer_science)) so you don't accidentally end up with more work than you should.

Use the analogy with blocks and modifiers from BEM when choosing between different entities and extensions. It helps me a lot to determine if I have a separate entity in front of me or an "extension modifier" if I think of it in a BEM context.

Interdependent Scenarios
The second big problem is related to each other user scripts, when an event from one script triggers another.

The only way I know of to deal with this that helps me is to split the scripts into smaller, atomic ones. Even if they are not called “custom”, but it will be easier to compose them.

In general, the problem of such scenarios is a consequence of another big problem in programming, entity [composition](https://en.wikipedia.org/wiki/Object_composition). A lot has already been written about what to do in order to effectively compose entities, and there is even a whole section of mathematics. We will not go far there, this is a topic for a separate post.

## Summary
In this post, I outlined and slightly expanded my report on clean architecture in the front-end.

This is not a gold standard, but rather a compilation of experience in different projects, paradigms and languages. It seems to me a convenient scheme that allows you to unlink the code and make independent layers, modules, services that can not only be deployed and published separately, but also transferred from project to project, if necessary.

We did not touch on OOP, since architecture and OOP are orthogonal things. Yes, the architecture talks about the composition of entities, but it does not dictate what exactly should be the composition unit: an object or a function. You can work with this scheme in different paradigms, which we have seen in the examples.

About OOP, I will only say that I recently wrote a post on how to use clean architecture along with OOP . If you are interested in the topic of architecture and you are not allergic to OOP, then I also advise you to look. In this post, we are writing a tree image generator on canvas.

To see exactly how this approach can be combined with other cool things like feature slicing, hexagonal architecture, CQS and others, I advise you to look at  DDD, Hexagonal, Onion, Clean, CQRS, … How I put it all together and the entire series of articles from this blog. Very to the point, short and to the point.
